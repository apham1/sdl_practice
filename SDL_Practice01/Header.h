#ifndef HEADER_H
#define HEADER_H

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>

class LTexture;
bool init();
bool loadMedia();
void close();

SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;
SDL_Rect gSprite[1];
std::string gFile = "Square.png";

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

class LTexture {
public:
	LTexture();
	~LTexture();
	bool loadFromFile(std::string file);
	void render(int x, int y, SDL_Rect* clip = NULL);
	int getWidth();
	int getHeight();
	void free();
private:
	SDL_Texture* mTexture;
	int mWidth;
	int mHeight;
};

#endif