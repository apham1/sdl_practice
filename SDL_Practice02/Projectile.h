#ifndef _PROJECTILE_H
#define _PROJECTILE_H

#include "ATexture.h"
#include "Constants.h"
#include "Particle.h"

class Projectile {
public:
	const double BULLET_SPEED = 8.0;

	Projectile();
	Projectile(ATexture* texture);
	~Projectile();

	void shoot(double angle, double centerX, double centerY);
	void move(SDL_Renderer* renderer);
	bool isActive();
	void setAngle(double angle);
	double getAngle();
	void setTexture(ATexture* texture);
	void render(SDL_Renderer* renderer, double projectileCenterX, double projectileCenterY, double angle);

private:
	Particle* particles[TOTAL_PARTICLES];
	void renderParticles();

	double mAngle, mPosX, mPosY, mCenterX, mCenterY;
	bool mActive;
	ATexture* mTexture;
};

#endif