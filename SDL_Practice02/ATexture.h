#ifndef _ATEXTURE_H
#define _ATEXTURE_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <string>

class ATexture {
public:
	ATexture();
	~ATexture();

	void render(SDL_Renderer* renderer, int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
	void loadMediaFromFile(SDL_Renderer* renderer, std::string file);
	void loadFontFromFile(SDL_Renderer* renderer, TTF_Font* font, std::string text, SDL_Color color);
	void free();

	int getWidth();
	int getHeight();

private: 
	SDL_Texture* mTexture;
	int mWidth;
	int mHeight;
};

#endif /* _ATEXTURE_H */