#include "Player.h"

Player::Player() {
	mPosX = 0;
	mPosY = 0;
	mVelX = 0;
	mVelY = 0;
}

Player::Player(ATexture* texture) : Player() {
	mTexture = texture;
}

void Player::handleEvent(SDL_Event& e) {
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
		switch (e.key.keysym.sym) {
			case SDLK_w: 
				mVelY -= PLAYER_SPEED;
				break;
			case SDLK_s:
				mVelY += PLAYER_SPEED;
				break;
			case SDLK_a:
				mVelX -= PLAYER_SPEED;
				break;
			case SDLK_d:
				mVelX += PLAYER_SPEED;
				break;
		}
	} else if (e.type == SDL_KEYUP && e.key.repeat == 0) {
		switch (e.key.keysym.sym) {
			case SDLK_w:
				mVelY += PLAYER_SPEED;
				break;
			case SDLK_s:
				mVelY -= PLAYER_SPEED;
				break;
			case SDLK_a:
				mVelX += PLAYER_SPEED;
				break;
			case SDLK_d:
				mVelX -= PLAYER_SPEED;
				break;
		}
	}
}

void Player::move() {
	mPosX += mVelX;

	if ((mPosX < 0) || (mPosX + PLAYER_WIDTH > LEVEL_WIDTH)) {
		mPosX -= mVelX;
	}

	mPosY += mVelY;

	if ((mPosY < 0) || (mPosY + PLAYER_HEIGHT > LEVEL_HEIGHT)) {
		mPosY -= mVelY;
	}
}

void Player::render(SDL_Renderer* renderer, int camX, int camY, SDL_Rect* clip, double angle) {
	mTexture->render(renderer, mPosX - camX, mPosY - camY, clip, angle);
}

int Player::getPosX(){
	return mPosX;
}

int Player::getPosY() {
	return mPosY;
}