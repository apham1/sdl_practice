#include "Projectile.h"

Projectile::Projectile() {
	mAngle = 0.0;
	mPosX = 0.0;
	mPosY = 0.0;
	mCenterX = 0.0;
	mCenterY = 0.0;
	mActive = false;
}

Projectile::Projectile(ATexture* texture) : Projectile() {
	mTexture = texture;
}

Projectile::~Projectile() {

}

void Projectile::shoot(double angle, double playerCenterX, double playerCenterY) {
	mAngle = angle;
	mCenterX = playerCenterX - mTexture->getWidth() / 2;
	mCenterY = playerCenterY - mTexture->getHeight() / 2;
	mActive = true;
}

void Projectile::move(SDL_Renderer* renderer) {
	mCenterX -= BULLET_SPEED * cos(mAngle * (M_PI / 180.0000));
	mCenterY -= BULLET_SPEED * sin(mAngle * (M_PI / 180.0000));

	isActive();

	render(renderer, mCenterX, mCenterY, mAngle);
}

bool Projectile::isActive() {
	if (mCenterX - mTexture->getWidth() / 2 > LEVEL_WIDTH || mCenterX < 0 || mCenterY - mTexture->getHeight() / 2 > LEVEL_HEIGHT || mCenterY < 0) {
		mActive = false;
	}
	return mActive;
}

void Projectile::setAngle(double angle) {
	mAngle = angle;
}

double Projectile::getAngle() {
	return mAngle;
}

void Projectile::setTexture(ATexture* texture) {
	mTexture = texture;
}

void Projectile::render(SDL_Renderer* renderer, double projectileCenterX, double projectileCenterY, double angle) {
	mTexture->render(renderer, projectileCenterX, projectileCenterY, NULL, angle + 180);
}