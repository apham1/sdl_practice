#ifndef _PARTICLE_H
#define _PARTICLE_H

#include "ATexture.h"

const int TOTAL_PARTICLES = 20;

class Particle {
public:
	Particle(int x, int y);
	void render();
	bool isDead();

private:
	int mPosX, mPosY;
	int mFrame;
	ATexture* mTexture;
};

#endif