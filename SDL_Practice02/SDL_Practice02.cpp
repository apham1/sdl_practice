#include "SDL_Practice02.h"

ATexture gPlayerTexture, gBGTexture, gBulletTexture, gMousePos, gTimeText;
Player gPlayer(&gPlayerTexture);
Projectile gProjectile(&gBulletTexture);
std::vector<Projectile> gBullets(30);
std::vector<Projectile>::iterator it;

SDL_Rect gCamera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };

SDL_Window* gWindow = NULL;
SDL_Renderer* gRenderer = NULL;

TTF_Font* gFont = NULL;
SDL_Color gBlack = { 0, 0, 0 };

const Uint8* gCurrentKeyStates = SDL_GetKeyboardState(NULL);

SDL_Joystick* gGameController = NULL;

SDL_Haptic* gControllerHaptic = NULL;

Mix_Chunk* gBulletSound = NULL;
Mix_Music* gMusic = NULL;

ATimer gTimer;
ATimer gFpsTimer;
std::stringstream timeText;

double joystickAngle = 0.0, xDir = 0.0, yDir = 0.0, angle = 0.0, bulletAngle = 0.0, bulletCenterX = 0.0, bulletCenterY = 0.0;
int playerCenterX = 0, playerCenterY = 0;
int gCountedFrames = 0, gLastFpsTick = 0, gFps = 0;
bool isBullet = false;

/** Controls (so far)
 *  WASD to move (or analog stick via controller)
 *  P to pause/play music and the game
 *  M to stop music
 *  Left-click to shoot
 *  Right-click to draw line
 *  Enter to reset timer at the top-right
 */

int main(int argc, char* args[]) {
	int mouseX = 0, mouseY = 0, lineStartX = 0, lineStartY = 0;
	float FPS = 0;
	bool drawLine = false;

	if (!init()) {
		printf("SDL could not initialize.");
	} else {
		bool quit = false;
		SDL_Event e;
		Mix_PlayMusic(gMusic, -1);

		while (!quit) {
			while (SDL_PollEvent(&e) != 0) {
				if (!gTimer.isPaused()) {
					if (e.type == SDL_QUIT || gCurrentKeyStates[SDL_SCANCODE_ESCAPE]) {
						quit = true;
					} else if (e.type == SDL_MOUSEMOTION) {
						SDL_GetMouseState(&mouseX, &mouseY);
					} else if (e.type == SDL_MOUSEBUTTONDOWN) {
						if (e.button.button == SDL_BUTTON_RIGHT) {
							drawLine = true;
							lineStartX = e.button.x;
							lineStartY = e.button.y;
						}

						if (e.button.button == SDL_BUTTON_LEFT) {
							for (it = gBullets.begin(); it != gBullets.end(); ++it) {
								if (!(*it).isActive()) {
									(*it).shoot(angle, playerCenterX, playerCenterY);
									Mix_PlayChannel(-1, gBulletSound, 0);
									break;
								}
							}
							//gProjectile.shoot(angle, playerCenterX, playerCenterY);
							//Mix_PlayChannel(-1, gBulletSound, 0);
						}
					} else if (e.type == SDL_MOUSEBUTTONUP && e.button.button == SDL_BUTTON_RIGHT) {
						drawLine = false;
					} else if (e.type == SDL_KEYDOWN) {
						switch (e.key.keysym.sym) {
							case SDLK_p:
								if (Mix_PlayingMusic() == 0) {
									Mix_PlayMusic(gMusic, -1);
								} else {
									if (Mix_PausedMusic() == 1) {
										Mix_ResumeMusic();
									} else {
										Mix_PauseMusic();
									}
								}

								if (gTimer.isPaused()) {
									gTimer.unpause();
								} else {
									gTimer.pause();
								}
								break;
							case SDLK_m:
								if (Mix_PlayingMusic() == 1) {
									Mix_HaltMusic();
								} else {
									Mix_PlayMusic(gMusic, -1);
								}
								break;
							case SDLK_BACKSPACE:
								if (gTimer.isStarted()) {
									gTimer.stop();
								} else {
									gTimer.start();
								}
						}
					} else if (e.type == SDL_JOYBUTTONDOWN) {
						if (e.jbutton.button == 0) {
							//shootBullet();
							//SDL_HapticRumblePlay(gControllerHaptic, 0.50, 250);
						}
					} else if (e.type == SDL_JOYAXISMOTION) {
						if (e.jaxis.which == 0) { //Motion on controller 0
							if (e.jaxis.axis == 0) { //X Axis Motion
								if (e.jaxis.value < -JOYSTICK_DEAD_ZONE) { //Left of Dead Zone
									xDir = -1;
								} else if (e.jaxis.value > JOYSTICK_DEAD_ZONE) { //Right of Dead Zone
									xDir = 1;
								} else {
									xDir = 0;
								}
							} else if (e.jaxis.axis == 1) { //Y Axis Motion
								if (e.jaxis.value < -JOYSTICK_DEAD_ZONE) {
									yDir = -1;
								} else if (e.jaxis.value > JOYSTICK_DEAD_ZONE) {
									yDir = 1;
								} else {
									yDir = 0;
								}
							}
						}
					}

					gPlayer.handleEvent(e);
				} else {
					if (e.type == SDL_QUIT || gCurrentKeyStates[SDL_SCANCODE_ESCAPE]) {
						quit = true;
					}

					if (e.type == SDL_KEYDOWN) {
						switch (e.key.keysym.sym) {
							case SDLK_p:
								if (Mix_PlayingMusic() == 0) {
									Mix_PlayMusic(gMusic, -1);
								} else {
									if (Mix_PausedMusic() == 1) {
										Mix_ResumeMusic();
									} else {
										Mix_PauseMusic();
									}
								}

								if (gTimer.isPaused()) {
									gTimer.unpause();
								} else {
									gTimer.pause();
								}
								break;
						}
					}
				}
			}

			//Keyboard WASD
			if (!gTimer.isPaused()) {
				gPlayer.move();

				gCamera.x = (gPlayer.getPosX() + Player::PLAYER_WIDTH / 2) - SCREEN_WIDTH / 2;
				gCamera.y = (gPlayer.getPosY() + Player::PLAYER_HEIGHT / 2) - SCREEN_HEIGHT / 2;

				if (gCamera.x < 0) {
					gCamera.x = 0;
				}

				if (gCamera.y < 0) {
					gCamera.y = 0;
				}

				if (gCamera.x > LEVEL_WIDTH - gCamera.w) {
					gCamera.x = LEVEL_WIDTH - gCamera.w;
				}

				if (gCamera.y > LEVEL_HEIGHT - gCamera.h) {
					gCamera.y = LEVEL_HEIGHT - gCamera.h;
				}

				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				SDL_RenderClear(gRenderer);

				gBGTexture.render(gRenderer, 0, 0, &gCamera);

				//Draw mouse coordinates at the top left of the screen.
				gMousePos.loadFontFromFile(gRenderer, gFont, (std::to_string(mouseX) + ", " + std::to_string(mouseY)).c_str(), gBlack);
				gMousePos.render(gRenderer, 0, 0);

				calculateFps();
				timeText.str("");
				timeText << gFps - 1;
				gTimeText.loadFontFromFile(gRenderer, gFont, timeText.str().c_str(), gBlack);
				gTimeText.render(gRenderer, gCamera.w - gTimeText.getWidth(), 0);

				playerCenterX = gPlayer.getPosX() + Player::PLAYER_WIDTH / 2 - gCamera.x;
				playerCenterY = gPlayer.getPosY() + Player::PLAYER_HEIGHT / 2 - gCamera.y;
				angle = atan2(playerCenterY - mouseY, playerCenterX - mouseX) * (180.0000 / M_PI);

				SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xFF);
				SDL_RenderDrawLine(gRenderer, playerCenterX, playerCenterY, mouseX, mouseY);

				for (it = gBullets.begin(); it != gBullets.end(); ++it) {
					if ((*it).isActive()) {
						(*it).move(gRenderer);
					}
				}

				/*if (gProjectile.isActive()) {
					gProjectile.move(gRenderer);
				}*/

				gPlayer.render(gRenderer, gCamera.x, gCamera.y, NULL, angle);

				if (drawLine) {
					SDL_RenderDrawLine(gRenderer, lineStartX, lineStartY, mouseX, mouseY);
				}

				SDL_RenderPresent(gRenderer);
				gCountedFrames++;
			} else {
				SDL_Delay(250);
			}
		}
	}
	free();

	return 0;
}

bool loadMedia() {
	bool success = true;
	SDL_Surface* image = NULL;

	gPlayerTexture.loadMediaFromFile(gRenderer, "laboon.png");
	gBulletTexture.loadMediaFromFile(gRenderer, "water.png");
	gBGTexture.loadMediaFromFile(gRenderer, "Background.png");
	/*gRedParticle.loadMediaFromFile(gRenderer, "red.bmp");
	gGreenParticle.loadMediaFromFile(gRenderer, "green.bmp");
	gBlueParticle.loadMediaFromFile(gRenderer, "blue.bmp");*/

	gFont = TTF_OpenFont("OpenSans-Regular.ttf", 14);

	gBulletSound = Mix_LoadWAV("shoot.wav");
	//gMusic = Mix_LoadMUS("");
	for (it = gBullets.begin(); it != gBullets.end(); ++it) {
		(*it).setTexture(&gBulletTexture);
	}

	Mix_VolumeMusic(50);
	
	SDL_FreeSurface(image);
	return success;
}

bool init() {
	bool success = true;

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_HAPTIC | SDL_INIT_AUDIO) < 0) {
		printf("SDL could not initialize.");
		success = false;
	} else {
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
			printf("Warning: Linear texture filtering not enabled!");
		}

		if (SDL_NumJoysticks() < 1) {
			printf("Warning: No joysticks connected!\n");
		} else {
			gGameController = SDL_JoystickOpen(0);
			if (gGameController == NULL) {
				printf("Warning: Unable to open game controller!");
			} else {
				gControllerHaptic = SDL_HapticOpenFromJoystick(gGameController);
				if (gControllerHaptic == NULL) {
					printf("Warning: Controller does not support haptics!");
				} else {
					if (SDL_HapticRumbleInit(gControllerHaptic) < 0) {
						printf("Warning: Unable to initialize rumble! SDL Error: %s\n", SDL_GetError());
					}
				}
			}
		}

		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
			printf("SDL_mixer could not initialize!");
			success = false;
		}

		gWindow = SDL_CreateWindow("SDL_Practice02", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			printf("Window could not be created");
			success = false;
		} else {
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (TTF_Init() == -1) {
				printf("TTF could not initialize.");
			} else {
				if (!loadMedia()) {
					success = false;
				}
			}
		}
	}
	return success;
}

void calculateFps() {
	if (gFpsTimer.getTicks() - gLastFpsTick > 1000) {
		gLastFpsTick = gFpsTimer.getTicks();
		gFps = gCountedFrames;
		gCountedFrames = 0;
	}
}

void free() {
	gPlayerTexture.free();
	gBulletTexture.free();
	gMousePos.free();

	SDL_JoystickClose(gGameController);
	SDL_HapticClose(gControllerHaptic);
	gGameController = NULL;
	gControllerHaptic = NULL;

	Mix_FreeChunk(gBulletSound);
	Mix_FreeMusic(gMusic);
	
	SDL_DestroyWindow(gWindow);
	SDL_DestroyRenderer(gRenderer);
	gWindow = NULL;
	gRenderer = NULL;

	Mix_Quit();
	SDL_Quit();
	IMG_Quit();
	TTF_Quit();
}