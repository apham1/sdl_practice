#include "ATexture.h"

ATexture::ATexture() {
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

ATexture::~ATexture() {
	free();
}

void ATexture::render(SDL_Renderer* renderer, int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip) {
	SDL_Rect renderQuad = { x, y, mWidth, mHeight };
	SDL_RenderCopyEx(renderer, mTexture, clip, &renderQuad, angle, center, flip);
}

void ATexture::loadMediaFromFile(SDL_Renderer* renderer, std::string file) {
	free();

	SDL_Surface* image = NULL;
	image = IMG_Load(file.c_str());
	SDL_SetColorKey(image, SDL_TRUE, SDL_MapRGB(image->format, 0x00, 0xFF, 0x40));

	mWidth = image->w;
	mHeight = image->h;

	mTexture = SDL_CreateTextureFromSurface(renderer, image);
	SDL_FreeSurface(image);
}

void ATexture::loadFontFromFile(SDL_Renderer* renderer, TTF_Font* font, std::string text, SDL_Color color) {
	free();

	SDL_Surface* textSurface = NULL;
	textSurface = TTF_RenderText_Solid(font, text.c_str(), color);

	mWidth = textSurface->w;
	mHeight = textSurface->h;

	mTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
	SDL_FreeSurface(textSurface);
}

void ATexture::free() {
	if (mTexture != NULL) {
		SDL_DestroyTexture(mTexture);
		mWidth = 0;
		mHeight = 0;
	}
}

int ATexture::getWidth() {
	return mWidth;
}

int ATexture::getHeight(){
	return mHeight;
}