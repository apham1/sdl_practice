#ifndef _PLAYER_H
#define _PLAYER_H

#include <math.h>
#include "ATexture.h"
#include "Constants.h"

class Player {
public:
	static const int PLAYER_WIDTH = 50;
	static const int PLAYER_HEIGHT = 50;
	static const int PLAYER_SPEED = 2;

	Player();
	Player(ATexture* texture);

	void handleEvent(SDL_Event& e);
	void move();
	void render(SDL_Renderer* renderer, int camX, int camY, SDL_Rect* clip, double angle);
	int getPosX();
	int getPosY();

private:
	ATexture* mTexture;
	int mPosX, mPosY;
	int mVelX, mVelY;
};

#endif