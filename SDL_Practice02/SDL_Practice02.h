#ifndef _SDL_PRACTICE02_H
#define _SDL_PRACTICE02_H

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <SDL_ttf.h>
#include <string>
#include <math.h>
#include <SDL_mixer.h>
#include <sstream>
#include <vector>
#include "ATexture.h"
#include "ATimer.h"
#include "Player.h"
#include "Particle.h"
#include "Projectile.h"

bool loadMedia();
bool init();
void free();
void calculateFps();

const int JOYSTICK_DEAD_ZONE = 8000;
const int SCREEN_FPS = 144;
const int SCREEN_TICKS_PER_FRAME = (1000 / SCREEN_FPS);

const double BULLET_SPEED = 8.0;

#endif /* _SDL_PRACTICE02_H */